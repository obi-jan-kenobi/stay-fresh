# stay-fresh

This is intended to be a service which looks at your repositories and its dependencies and creates a pull request on its behalf with updated dependencies

## Todo
- query (gitlab, github) [Rest or GQL?]
- resolve dependencies for languages (nodejs, scala, go)
- update dependencies
- git commit
- create pull-requests
- scheduling

{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE DataKinds #-}

module Provider.Provider where

import Provider.Types
import qualified Data.Aeson as A
import Network.HTTP.Client
import Network.HTTP.Types.Header
import Network.HTTP.Client.TLS (tlsManagerSettings)
import Data.Text
import qualified Data.Text.Lazy.Encoding as T
import qualified Data.ByteString.Lazy as Lazy
import qualified Data.ByteString.Base64 as Base64
import qualified Data.ByteString.Char8 as Char8
import System.Environment
import Data.Text.Encoding

getContent :: IO ()
getContent = do
    manager <- newManager tlsManagerSettings
    token <- getEnv "GITHUB_TOKEN"
    initialRequest <- parseRequest "https://api.github.com/repos/obi-jan-kenobi/todomato/contents/package.json"
    let request = initialRequest {
        method = "GET",
        requestHeaders =
            [ (hUserAgent, "stay-fresh")
            , (hAuthorization, encodeUtf8 "token " <> encodeUtf8 (Data.Text.pack token))
            , (hContentEncoding, "application/json")
            , (hAccept, "application/vnd.github.v3+json")
            ] }
    response <- httpLbs request manager
    let contentresponse = content $ (\x -> case x of
            Nothing -> ContentResponse { content = "" } 
            Just cr -> cr) (A.decode (responseBody response))
    let bs = Char8.pack contentresponse
    print $ Base64.decode bs

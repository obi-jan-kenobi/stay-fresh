{-# LANGUAGE OverloadedStrings #-}

module Provider.Types where

import Prelude
import qualified Data.Map as Map
import Data.Aeson

type Owner = String
type Name = String
type Url = String
type Version = String

data Provider = GitHub | GitLab

data ContentResponse = ContentResponse
    { content :: String
    }

instance FromJSON ContentResponse where
    parseJSON (Object v) = ContentResponse <$> v .: "content"

data Repository = Repository Name
data Language = NodeJS | Scala | Go
data Dependencies = Dependencies Language (Map.Map Name Version)

contentPath :: Provider -> Owner -> Repository -> String
contentPath GitHub o (Repository r) = "/repos/" <> o <> "/" <> r <> "/contents/"
contentPath GitLab o r = ""


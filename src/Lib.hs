module Lib
    ( someFunc
    , Language
    , Dependency
    ) where

someFunc :: IO ()
someFunc = putStrLn "someFunc"

data Language
    = NodeJS
    | Scala
    | Go

newtype Version = Version String

data Dependency = Dependency Language String Version

lang :: String -> Language
lang = undefined

dependencies :: Language -> [Dependency]
dependencies = undefined

update :: [Dependency] -> [Dependency]
update = undefined